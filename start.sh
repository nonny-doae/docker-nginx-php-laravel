#!/usr/bin/env bash

set -e

role=${CONTAINER_ROLE:-app}
env=${APP_ENV:-production}

# if [ "$env" != "local" ]; then
#     echo "Caching configuration..."
#     (cd /var/www/html && php artisan config:cache && php artisan view:cache)
# fi

if [ "$role" = "app" ]; then

    exec /init

elif [ "$role" = "queue" ]; then

    echo "Running the default queue..."
    php /var/www/html/artisan queue:work --verbose --tries=3 --timeout=90

elif [ "$role" = "logger" ]; then

    echo "Running the logger queue..."
    php /var/www/html/artisan queue:work --queue=logging,logging-error --verbose --tries=3 --timeout=3

elif [ "$role" = "scheduler" ]; then

    while [ true ]
    do
      php /var/www/html/artisan schedule:run --verbose --no-interaction &
      sleep 60
    done

else
    echo "Could not match the container role \"$role\""
    exit 1
fi